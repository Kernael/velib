import sys
import os
import yaml

path = os.path.dirname(os.path.realpath("__file__")) + '/'

def data_directory_path(etape):
    return path + 'etape' + str(etape) + '/data/'

def history_file_path():
    return path + 'histories_2week.csv'

def stations_file_path():
    return path + 'stations.csv'

def date_format():
    return "%Y-%m-%dT%H:%M:%S.000Z"

def dump_data(step, data, filename, hour = '18'):        
    with open(data_directory_path(step)+filename+'_'+hour+'h.yml', 'w') as outfile:
        outfile.write(yaml.dump(data, default_flow_style=False))
