#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os

from datetime import *
from pyspark import SparkContext

#
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

import helper
#

sc = SparkContext()

history = sc.textFile(helper.history_file_path())
stations = sc.textFile(helper.stations_file_path())

history_header = history.first()
stations_header = stations.first()

#########################################

class Step:
    def __init__(self):
        pass

    def run(self, hour):
        self.available_bikes = history\
            .filter(lambda line: line != history_header)\
            .map(lambda line: line.split(','))\
            .filter(lambda line: len(line) > 1)\
            .filter(lambda line: datetime\
                .strptime(line[3], helper.date_format()).hour == int(hour))\
            .map(lambda line: (line[0],line[2]))\
            .collect()

        self.available_stands = history\
            .filter(lambda line: line != history_header)\
            .map(lambda line: line.split(','))\
            .filter(lambda line: len(line) > 1)\
            .filter(lambda line: datetime\
                .strptime(line[3], helper.date_format()).hour == int(hour))\
            .map(lambda line: (line[0],line[1]))\
            .collect()

        self.result_bikes = dict((x.encode('utf-8'), y.encode('utf-8')) for x, y in self\
                           .available_bikes)
        helper.dump_data(1, self.result_bikes, 'available_bikes', hour)

        self.result_stands = dict((x.encode('utf-8'), y.encode('utf-8')) for x, y in self\
                                  .available_stands)
        helper.dump_data(1, self.result_stands, 'available_stands', hour)

#########################################

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage : step.py hour"
        sys.exit(0)

    step = Step()
    step.run(sys.argv[1])
